## Description

Client for `wsserver`. 
Receives data from websocket from `exchanges` topic.

## Prerequisites

Node and npm must be installed on machine.

## Instalation

1. Install expo globally:

```shell
npm install --global expo-cli
```

(details: https://docs.expo.io/)

2. Inside this projects folder run:

```shell
yarn install
```

...and wait until it's done

## Running app

!!! wsserver has to be running. !!!

1. Run:

```shell
yarn start
```

Metro server should start (opens with default browsers).

2. Start simulator (iphone/android) with corresponding tools (xcode/android ide)

* when simulator is running, from the shell, where `yarn start`
   was called, press `i` -> for iphone simulator, or `a` -> for android simulator.

3. or install on your iphone/android `expo` client from your phone store
and scan the code from Metro server page (see point 1).


