import React from "react";
import {View, Dimensions} from "react-native";
import {LineChart} from "react-native-chart-kit";

const ChartDemo = (props) => {

    let postProcessedArray = props.data;

    return (
        <View>
            { Object.keys(postProcessedArray).length > 0 && <LineChart
                data={{
                    labels: Object.keys(postProcessedArray),
                    datasets: [
                        {
                            data: Object.values(postProcessedArray)
                        }
                    ]
                }}
                width={Dimensions.get('window').width - 16} // from react-native
                height={220}
                yAxisLabel={'$'}
                chartConfig={{
                    backgroundColor: '#1cc910',
                    backgroundGradientFrom: '#eff3ff',
                    backgroundGradientTo: '#efefef',
                    decimalPlaces: 2, // optional, defaults to 2dp
                    color: (opacity = 255) => `rgba(0, 0, 0, ${opacity})`,
                    style: {
                        borderRadius: 16,
                    },
                }}
                bezier
                style={{
                    marginVertical: 8,
                    borderRadius: 16,
                }}
            />}
        </View>
    )
}

export default ChartDemo;
