import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import moment from "moment";
import ChartDemo from "./chartdemo";

export default function App() {

  const [connected, setConnected] = useState(false);
  const [tmpArray, setTmpArray] = useState([]);
  const [postProcessedArray, setPostProcessedArray] = useState({});

    let removeDuplicates = (originalArray, prop) => {
        const newArray = [];
        const lookupObject  = {};

        for(const i in originalArray) {
            lookupObject[originalArray[i][prop]] = originalArray[i];
        }

        for(const i in lookupObject) {
            newArray.push(lookupObject[i]);
        }
        return newArray;
    }

  useEffect(() => {
      let mounted = true;
      // NOTE: connect to WebSocket server
      const socket = new WebSocket("ws://192.168.1.24:3210");
      socket.onopen = () => {
          setConnected(true);
      }
      socket.onmessage = (e) => {
          const rawData = {
             value: e.data,
             time: moment().format("hh:mm:ss"),
          }
          let array = tmpArray;
          array.push(rawData);
          setTmpArray(array);
          const unique = removeDuplicates(tmpArray, "time");
          let array2Process = unique;
          if(unique.length > 8) {
              array2Process = unique.slice(Math.max(unique.length - 8, 1))
          }
          let afterProcessing = {};
          array2Process.forEach(item => {
              afterProcessing[item.time] = parseFloat(item.value);
          })
          setPostProcessedArray(afterProcessing);
      }

      return function cleanup() {
          mounted = false
      }
  }, []);

  return (
    <View style={styles.container}>
      <Text>connected: {connected ? 'true' : 'false'}</Text>
        <ChartDemo
            data={postProcessedArray}
        />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
